var cors = require('cors')



var express = require('express');
var bodyParser = require('body-parser');
var mongoose =  require('mongoose');

var Product = require('./model/product');
var WishList = require('./model/wishlist');

var app = express();
var db = mongoose.connect('mongodb://localhost:27017/shop-db',{ useNewUrlParser: true });
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

app.post('/product', function(request,response){
	var product = new Product();
	product.name = request.body.name;
	product.price = request.body.price;
	product.save(function(err, savedProduct){
		if(err){
			response.status(500).send({error:"Could not save product"});
		}else{
			response.status(200).send(savedProduct);
		}
	});
});

app.get('/product', function(request, response){
	Product.find({},function(err, products){
		if(err){
			response.status(500).send({error:"Couldnt retrieve data"});
		}
		else{
			response.status(200).send(products);
		}
	});
});

app.post('/wishlist', function(request,response){
	var wishList = new WishList(request.body)
	wishList.save(function(err,savedList){
		if(err){
			response.status(500).send({error:"Could not create wishlist"});
		}else{
			response.send(savedList);
		}
	});
	
	
});

app.put('/wishlist/product/add', function(request, response){
	Product.findOne({_id: request.body.productId}, function(err, product){
		if(err){
			response.status(500).send({error:"Could not create wishlist"});
		}else{
			WishList.update({_id:request.body.wishListId}, {$addToSet: {products: product._id}},function(err,newWishList){
				if(err){
					response.status(500).send({error:"Could not create wishlist"});
				}else{
					response.send(newWishList);
				}
			});
		}
	})
});

app.get('/wishlist', function(request, response){
	WishList.find({}).populate({path:'products',model:'Product'}).exec(function(err, wishlists){
		if(err){
			response.status(500).send({error:"Couldnt retrieve data"});
		}
		else{
			response.status(200).send(wishlists);
		}
	});
});

app.listen(3004, function() {
	console.log("Shop Test");
});

