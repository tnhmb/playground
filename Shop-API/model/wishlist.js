// JavaScript Document
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var objectID = mongoose.Schema.Types.ObjectId;

var wishList = new Schema({
	title: {type: String, default: "Cool Wish List"},
	products: [{type: objectID, ref:'Product'}]
});

module.exports = mongoose.model('WishList', wishList)