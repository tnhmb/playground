// JavaScript Document
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var product = new Schema({
	name: String,
	price: Number,
	likes: {type: Number, default: 0}
});

module.exports = mongoose.model('Product', product)