import React, { Component } from 'react';
import logo from './sun.png';
import './App.css';
import Product from '../product/product.js';
import HttpService from '../services/http-service'
import { Button } from 'reactstrap'; 

const http = new HttpService();
class App extends Component {
	
constructor(props){
	super(props);
	
	this.state = {products:[]};
	
	this.loadData = this.loadData.bind(this);
	this.prodcuctList = this.prodcuctList.bind(this);
	this.loadData();
	
}

	prodcuctList = () =>{
		const list = this.state.products.map((product) =>
			<div className="col-sm-4 App-main " key = {product._id}>
				<Product price={product.price} title={product.name} imgUrl={product.imgUrl} />
			</div>
		);
		return (list);
	}
	
loadData = () => {
	 var self = this;
	http.getProducts().then(data => {
		self.setState({products: data})
	}, err => {
		
	});
}	
	
render() {
	return (
	  <div className="App">
		<header className="App-header">
		  <img src={logo} className="App-logo" alt="logo" />
		  <h1 className="App-title">Welcome to The Shop</h1>
		</header>
			<div className="jumbotron ">
				<div className="container">
					<div className="row  " >
						<div className="col"></div>
						<div className="col-md-8">
							<div className="col"></div>
								{this.prodcuctList()}
							<div className="col"></div>
						</div>
						<div className="col"></div>
						
						
						
					</div>
				</div>
				
			</div>
			
		<p className="App-intro">
		  To get started, edit <code>src/App.js</code> and save to reload.
		</p>
	  </div>
	);
  }
}

export default App;
