import React, {Component} from 'react';
import './product.css';
import { Button } from 'reactstrap'; 

class Product extends Component {
	render(){
		return(
			<div className="card product ">
				<img className="card-img-top" src={this.props.imgUrl} alt="Product"></img>
				<div className="card-body">
					<h2 className="card-title">{this.props.title}</h2>
					<h3 className="card-text">Price: {this.props.price} $</h3>
					<a href="#" className="btn btn-primary">Add to Wishlist</a>
				</div>
			</div>
			);
		
	}
}
export default Product;